
## About Twitter Poster

This application created using **Laravel Framework 5.8.19**.

*This application help users to login using there twitter account and post tweets into there timeline.*

Do the following configuration before you start using it: 
- After clone the repository into your machine.
- cd twitter_poster.
- Run **cp .env.example .env**.
- Run **php artisan key:generate**.
- Setup .env file and put your database credentials.
- Run **composer install**.
- Run **php artisan migrate**.
- Setup you custom V-Host, or simply run **php artisan serve**.
- Set the callback URL in your .env file based on your application URL



