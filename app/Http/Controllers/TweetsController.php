<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Thujohn\Twitter\Facades\Twitter;

class TweetsController extends Controller
{
    public function Tweet(Request $request){
        $data = $request->all();
        if(isset($data['tweet']) && !empty($data['tweet'])){
            $token = Auth::user()->token;
            $secret = Auth::user()->tokenSecret;
           try{
               /** Post tweets on behalf of logged in user */
               Twitter::reconfig(['token' => $token, 'secret' => $secret]);

               Twitter::postTweet(['status' => $data['tweet'], 'format' => 'json']);
               return Redirect::back()->with('success', 'Your tweet has successfully posted');
           }catch (\Exception $exception){
               return Redirect::back()->with('warning', $exception->getMessage());
           }
        }else{
            return Redirect::back()->with('warning', 'Kindly write a post, don\'t leave it empty');
        }

    }
}
