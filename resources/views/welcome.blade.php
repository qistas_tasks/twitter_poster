<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link rel="shortcut icon" href="https://help.twitter.com/content/dam/help-twitter/brand/logo.png">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.5.js"></script>
    <script>
        function countChar(val) {
            var len = val.value.length;
            if (len >= 280) {
                val.value = val.value.substring(0, 280);
            } else {
                $('#charNum').text(280 - len);
            }
        };

        $("document").ready(function () {
            setTimeout(function () {
                $(".flash-message").remove();
            }, 2500); // 5 secs

        });
    </script>
    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 50px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        a {
            text-decoration: none !important;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref">
    <div class="flex-center box">
        @auth
            <div class="top-right links">
                <a href="{{ url('/auth/logout') }}">Logout</a></p>
            </div>
        @endauth
    </div>
    <div class="content bg-info col-md-5 full-height">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
                @endif
            @endforeach
        </div>
        <div class="title">
            <img src="https://help.twitter.com/content/dam/help-twitter/brand/logo.png" width="20%">
            <div class="">
                <p>Twitter Poster</p>
            </div>
            @auth
                <div class="">
                    <h3>Welcome <b>{{ Auth::user()->name }}</b></h3>
                </div>
            @else
                <div class="card">
                    <div class="card-header m-b-md">
                        <h3>
                            Enjoy posting tweets into your time line
                        </h3>
                    </div>
                    <div class="card-body m-t-20">
                        <a class="'btn btn-lg btn-primary" href="{{ url('auth/redirect/twitter') }}">Login</a>
                    </div>
                </div>
            @endauth
        </div>
        <div class="links">
            @if(Session::has('success'))
                <div class="flash-message">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <p class="alert alert-success">{{ Session::get( 'success' ) }}</p>
                </div>
            @elseif(Session::has('warning'))
                <div class="flash-message">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <p class="alert alert-warning">{{ Session::get( 'warning' ) }}</p>
                </div>
            @endif
            @auth
                <div class="row col-lg-12">
                    {{ Form::open(array('url' => '/tweet')) }}
                    <label>{{Form::label('tweet', 'Write your tweet')}}</label>

                    <div class="form-group">
                        {{Form::textarea('tweet', '', ['onkeyup' => "countChar(this)"])}}
                        <label>You have <b><span id="charNum"> 280</span></b> characters left</label>
                    </div>
                    <div class="form-group">
                        {!! Form::submit('Tweet', ['class' => 'btn btn-lg btn-primary'] ) !!}
                    </div>

                    {{ Form::close() }}
                </div>
            @endauth
        </div>
    </div>
</div>
</body>
</html>
